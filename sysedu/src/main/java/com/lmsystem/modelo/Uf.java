package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "uf")
@NamedQuery(name = "Uf.findAll", query = "SELECT u FROM Uf u")
public class Uf implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkuf;

	@Column(nullable = false, columnDefinition = "text")
	private String sigla;

	@Column(nullable = false, columnDefinition = "text")
	private String uf;

	@Column(nullable = false, length = 4)
	private String codibge;

	@OneToMany(mappedBy = "uf", orphanRemoval = true)
	private List<Municipio> municipios;

	@ManyToOne
	@JoinColumn(name = "fkpais", nullable = false, referencedColumnName = "pkpais")
	private Pais pais;

	public Uf() {
	}

	public Integer getPkuf() {
		return this.pkuf;
	}

	public void setPkuf(Integer pkuf) {
		this.pkuf = pkuf;
	}

	public String getSigla() {
		return this.sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getUf() {
		return this.uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCodibge() {
		return codibge;
	}

	public void setCodibge(String codibge) {
		this.codibge = codibge;
	}

	public List<Municipio> getMunicipios() {
		return this.municipios;
	}

	public void setMunicipios(List<Municipio> municipios) {
		this.municipios = municipios;
	}

	public Municipio addMunicipio(Municipio municipio) {
		getMunicipios().add(municipio);
		municipio.setUf(this);

		return municipio;
	}

	public Municipio removeMunicipio(Municipio municipio) {
		getMunicipios().remove(municipio);
		municipio.setUf(null);

		return municipio;
	}

	public Pais getPais() {
		return this.pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkuf == null) ? 0 : pkuf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Uf other = (Uf) obj;
		if (pkuf == null) {
			if (other.pkuf != null)
				return false;
		} else if (!pkuf.equals(other.pkuf))
			return false;
		return true;
	}

}