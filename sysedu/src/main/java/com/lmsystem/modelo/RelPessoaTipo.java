package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "rel_pessoa_tipo")
@NamedQuery(name = "RelPessoaTipo.findAll", query = "SELECT r FROM RelPessoaTipo r")
public class RelPessoaTipo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer pkrelpessoatipo;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fkpessoafisica")
	private PessoaFisica pessoaFisica;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fkpessoajuridica")
	private PessoaJuridica pessoaJuridica;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false, columnDefinition = "text")
	private TipoPessoa tipoPessoa;

	public RelPessoaTipo() {
	}

	public Integer getPkrelpessoatipo() {
		return pkrelpessoatipo;
	}

	public void setPkrelpessoatipo(Integer pkrelpessoatipo) {
		this.pkrelpessoatipo = pkrelpessoatipo;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	public TipoPessoa getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(TipoPessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkrelpessoatipo == null) ? 0 : pkrelpessoatipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RelPessoaTipo other = (RelPessoaTipo) obj;
		if (pkrelpessoatipo == null) {
			if (other.pkrelpessoatipo != null)
				return false;
		} else if (!pkrelpessoatipo.equals(other.pkrelpessoatipo))
			return false;
		return true;
	}

}