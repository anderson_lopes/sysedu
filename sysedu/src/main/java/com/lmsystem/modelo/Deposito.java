package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "deposito")
@NamedQuery(name = "Deposito.findAll", query = "SELECT d FROM Deposito d")
public class Deposito implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkdeposito;

	@Column(nullable = false, columnDefinition = "text")
	private String deposito;

	@ManyToOne
	@JoinColumn(name = "fkdepositorelacionado", referencedColumnName = "pkdeposito")
	private Deposito depositoBean;

	@OneToMany(mappedBy = "depositoBean", orphanRemoval = true)
	private List<Deposito> depositos;

	public Deposito() {
	}

	public Integer getPkdeposito() {
		return this.pkdeposito;
	}

	public void setPkdeposito(Integer pkdeposito) {
		this.pkdeposito = pkdeposito;
	}

	public String getDeposito() {
		return this.deposito;
	}

	public void setDeposito(String deposito) {
		this.deposito = deposito;
	}

	public Deposito getDepositoBean() {
		return this.depositoBean;
	}

	public void setDepositoBean(Deposito depositoBean) {
		this.depositoBean = depositoBean;
	}

	public List<Deposito> getDepositos() {
		return this.depositos;
	}

	public void setDepositos(List<Deposito> depositos) {
		this.depositos = depositos;
	}

	public Deposito addDeposito(Deposito deposito) {
		getDepositos().add(deposito);
		deposito.setDepositoBean(this);

		return deposito;
	}

	public Deposito removeDeposito(Deposito deposito) {
		getDepositos().remove(deposito);
		deposito.setDepositoBean(null);

		return deposito;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkdeposito == null) ? 0 : pkdeposito.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Deposito other = (Deposito) obj;
		if (pkdeposito == null) {
			if (other.pkdeposito != null)
				return false;
		} else if (!pkdeposito.equals(other.pkdeposito))
			return false;
		return true;
	}

}