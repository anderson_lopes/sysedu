package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "nacionalidade")
@NamedQuery(name = "Nacionalidade.findAll", query = "SELECT n FROM Nacionalidade n")
public class Nacionalidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pknacionalidade;

	@Column(columnDefinition = "text")
	private String codnacionalidade;

	@Column(nullable = false, columnDefinition = "text", unique = true)
	private String nacionalidade;

	@ManyToOne
	@JoinColumn(name = "fkpais", nullable = false, referencedColumnName = "pkpais")
	private Pais pais;

	@OneToMany(mappedBy = "nacionalidade", orphanRemoval = true)
	private List<PessoaFisica> pessoaFisicas;

	public Nacionalidade() {
	}

	public Integer getPknacionalidade() {
		return this.pknacionalidade;
	}

	public void setPknacionalidade(Integer pknacionalidade) {
		this.pknacionalidade = pknacionalidade;
	}

	public String getCodnacionalidade() {
		return this.codnacionalidade;
	}

	public void setCodnacionalidade(String codnacionalidade) {
		this.codnacionalidade = codnacionalidade;
	}

	public String getNacionalidade() {
		return this.nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public Pais getPais() {
		return this.pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public List<PessoaFisica> getPessoaFisicas() {
		return this.pessoaFisicas;
	}

	public void setPessoaFisicas(List<PessoaFisica> pessoaFisicas) {
		this.pessoaFisicas = pessoaFisicas;
	}

	public PessoaFisica addPessoaFisica(PessoaFisica pessoaFisica) {
		getPessoaFisicas().add(pessoaFisica);
		pessoaFisica.setNacionalidade(this);

		return pessoaFisica;
	}

	public PessoaFisica removePessoaFisica(PessoaFisica pessoaFisica) {
		getPessoaFisicas().remove(pessoaFisica);
		pessoaFisica.setNacionalidade(null);

		return pessoaFisica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pknacionalidade == null) ? 0 : pknacionalidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nacionalidade other = (Nacionalidade) obj;
		if (pknacionalidade == null) {
			if (other.pknacionalidade != null)
				return false;
		} else if (!pknacionalidade.equals(other.pknacionalidade))
			return false;
		return true;
	}

}