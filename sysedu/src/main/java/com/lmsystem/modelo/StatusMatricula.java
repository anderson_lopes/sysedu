package com.lmsystem.modelo;

public enum StatusMatricula {

	ATIVO("Ativo"), CANCELADO("Cancelado"), DESATIVADO("Desativado");

	private String descricao;

	StatusMatricula(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
