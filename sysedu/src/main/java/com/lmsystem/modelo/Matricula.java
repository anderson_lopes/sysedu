package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "matricula")
@NamedQuery(name = "Matricula.findAll", query = "SELECT m FROM Matricula m")
public class Matricula implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer pkmatricula;

	@NotEmpty
	@Column(columnDefinition = "text", unique = true)
	private String matricula;

	@ManyToOne
	@JoinColumn(name = "fkpessoafisica", nullable = false)
	private PessoaFisica pessoaFisica;

	@NotNull
	@Past(message = "Data da matrícula superior à data atual")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date datamatricula;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false, columnDefinition = "text")
	private StatusMatricula statusMatricula;

	public Integer getPkmatricula() {
		return pkmatricula;
	}

	public void setPkmatricula(Integer pkmatricula) {
		this.pkmatricula = pkmatricula;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public Date getDatamatricula() {
		return datamatricula;
	}

	public void setDatamatricula(Date datamatricula) {
		this.datamatricula = datamatricula;
	}

	public StatusMatricula getStatusMatricula() {
		return statusMatricula;
	}

	public void setStatusMatricula(StatusMatricula statusMatricula) {
		this.statusMatricula = statusMatricula;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkmatricula == null) ? 0 : pkmatricula.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Matricula other = (Matricula) obj;
		if (pkmatricula == null) {
			if (other.pkmatricula != null)
				return false;
		} else if (!pkmatricula.equals(other.pkmatricula))
			return false;
		return true;
	}

}
