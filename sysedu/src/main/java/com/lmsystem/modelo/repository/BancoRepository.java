package com.lmsystem.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Banco;

public class BancoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Banco obterPorID(Integer pkbanco) {
		return manager.find(Banco.class, pkbanco);
	}

	public Banco guardar(Banco banco) {
		return manager.merge(banco);
	}

	public List<Banco> listarTodos() {
		return manager.createQuery("from Banco order by pkbanco desc", Banco.class).getResultList();
	}

	public void remover(Banco banco) {
		manager.remove(manager.getReference(Banco.class, banco.getPkbanco()));
	}

	public List<Banco> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from Banco where " + campo + " like :value order by banco", Banco.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<Banco> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Banco where " + campo + " = :value order by banco", Banco.class)
				.setParameter("value", value).getResultList();
	}

}
