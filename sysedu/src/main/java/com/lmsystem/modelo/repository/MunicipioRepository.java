package com.lmsystem.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Municipio;

public class MunicipioRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Municipio obterPorID(Integer pkmunicipio) {
		return manager.find(Municipio.class, pkmunicipio);
	}

	public Municipio guardar(Municipio entity) {
		return manager.merge(entity);
	}

	public void remover(Municipio entity) {
		manager.remove(manager.getReference(Municipio.class, entity.getPkmunicipio()));
	}

	public List<Municipio> listarTodos() {
		return manager.createQuery("from Municipio order by pkmunicipio desc", Municipio.class).getResultList();
	}

	public List<Municipio> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from Municipio where " + campo + " like :value", Municipio.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<Municipio> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Municipio where " + campo + " = :value", Municipio.class)
				.setParameter("value", value).getResultList();
	}

}
