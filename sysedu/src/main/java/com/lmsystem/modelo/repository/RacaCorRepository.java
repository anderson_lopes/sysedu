package com.lmsystem.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.RacaCor;

public class RacaCorRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public RacaCor obterPorID(Integer pkracacor) {
		return manager.find(RacaCor.class, pkracacor);
	}

	public RacaCor guardar(RacaCor entity) {
		return manager.merge(entity);
	}

	public void remover(RacaCor entity) {
		manager.remove(manager.getReference(RacaCor.class, entity.getPkracacor()));
	}

	public List<RacaCor> listarTodos() {
		return manager.createQuery("from RacaCor order by pkracacor desc", RacaCor.class).getResultList();
	}

	public List<RacaCor> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from RacaCor where " + campo + " like :value order by racacor", RacaCor.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<RacaCor> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from RacaCor where " + campo + " = :value order by racacor", RacaCor.class)
				.setParameter("value", value).getResultList();
	}

}
