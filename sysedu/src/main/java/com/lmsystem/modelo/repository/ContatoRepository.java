package com.lmsystem.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Contato;

public class ContatoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Contato obterPorID(Integer pkcontato) {
		return manager.find(Contato.class, pkcontato);
	}

	public Contato guardar(Contato contato) {
		return manager.merge(contato);
	}

	public void remover(Contato contato) {
		manager.remove(manager.getReference(Contato.class, contato.getPkcontato()));
	}

	public List<Contato> listarTodos() {
		return manager.createQuery("from Contato order by pkcontato desc", Contato.class).getResultList();
	}

	public List<Contato> buscarPorDescricao(String campo, String value) {
		return manager
				.createQuery("from Contato where " + campo + " like :value order by pkcontato desc", Contato.class)
				.setParameter("value", "%" + value + "%").getResultList();
	}

	public List<Contato> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Contato where " + campo + " = :value order by pkcontato desc", Contato.class)
				.setParameter("value", value).getResultList();
	}

}
