package com.lmsystem.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.TipoDocumento;

public class TipoDocumentoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public TipoDocumento obterPorID(Integer pktipodocumento) {
		return manager.find(TipoDocumento.class, pktipodocumento);
	}

	public TipoDocumento guardar(TipoDocumento entity) {
		return manager.merge(entity);
	}

	public void remover(TipoDocumento entity) {
		manager.remove(manager.getReference(TipoDocumento.class, entity.getPktipodocumento()));
	}

	public List<TipoDocumento> listarTodos() {
		return manager.createQuery("from TipoDocumento order by pktipodocumento desc", TipoDocumento.class)
				.getResultList();
	}

	public List<TipoDocumento> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from TipoDocumento where " + campo + " like :value order by tipodocumento",
				TipoDocumento.class).setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<TipoDocumento> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from TipoDocumento where " + campo + " = :value order by tipodocumento",
				TipoDocumento.class).setParameter("value", value).getResultList();
	}

}
