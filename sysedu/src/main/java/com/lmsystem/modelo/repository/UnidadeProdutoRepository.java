package com.lmsystem.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.UnidadeProduto;

public class UnidadeProdutoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public UnidadeProduto obterPorID(Integer pkunidade) {
		return manager.find(UnidadeProduto.class, pkunidade);
	}

	public UnidadeProduto guardar(UnidadeProduto entity) {
		return manager.merge(entity);
	}

	public void remover(UnidadeProduto entity) {
		manager.remove(manager.getReference(UnidadeProduto.class, entity.getPkunidade()));
	}

	public List<UnidadeProduto> listarTodos() {
		return manager.createQuery("from UnidadeProduto order by pkunidade desc", UnidadeProduto.class).getResultList();
	}

	public List<UnidadeProduto> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from UnidadeProduto where " + campo + " like :value order by pkunidade desc",
				UnidadeProduto.class).setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<UnidadeProduto> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from UnidadeProduto where " + campo + " = :value order by pkunidade desc",
				UnidadeProduto.class).setParameter("value", value).getResultList();
	}
}
