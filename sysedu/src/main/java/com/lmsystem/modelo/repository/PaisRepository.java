package com.lmsystem.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Pais;

public class PaisRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Pais obterPorID(Integer pkpais) {
		return manager.find(Pais.class, pkpais);
	}

	public Pais guardar(Pais entity) {
		return manager.merge(entity);
	}

	public void remover(Pais entity) {
		manager.remove(manager.getReference(Pais.class, entity.getPkpais()));
	}

	public List<Pais> listarTodos() {
		return manager.createQuery("from Pais order by pais", Pais.class).getResultList();
	}

	public List<Pais> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from Pais where " + campo + " like :value order by pais", Pais.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<Pais> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Pais where " + campo + " = :value order by pais", Pais.class)
				.setParameter("value", value).getResultList();
	}

}
