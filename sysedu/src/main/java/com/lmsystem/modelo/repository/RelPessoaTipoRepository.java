package com.lmsystem.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.PessoaFisica;
import com.lmsystem.modelo.RelPessoaTipo;
import com.lmsystem.modelo.TipoPessoa;

public class RelPessoaTipoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public RelPessoaTipo obterPorID(Integer pk) {
		return manager.find(RelPessoaTipo.class, pk);
	}

	public RelPessoaTipo guardar(RelPessoaTipo relPessoaTipo) {
		return manager.merge(relPessoaTipo);
	}

	public List<RelPessoaTipo> listarTodos() {
		return manager.createNamedQuery("RelPessoaTipo.findAll", RelPessoaTipo.class).getResultList();
	}

	public List<RelPessoaTipo> listarTodos(TipoPessoa tipoPessoa) {
		return manager.createQuery("from RelPessoaTipo r where r.tipoPessoa = :tipoPessoa order by r.pessoaFisica.nome",
				RelPessoaTipo.class).setParameter("tipoPessoa", tipoPessoa).getResultList();
	}

	public void remover(RelPessoaTipo relPessoaTipo) {
		manager.remove(manager.getReference(RelPessoaTipo.class, relPessoaTipo.getPkrelpessoatipo()));
	}

	public List<RelPessoaTipo> buscarPorRelacionamento(PessoaFisica pessoaFisica, TipoPessoa tipoPessoa) {
		return manager
				.createQuery(
						"from RelPessoaTipo where pessoaFisica = :pessoaFisica and tipoPessoaFisica = :tipoPessoaFisica",
						RelPessoaTipo.class)
				.setParameter("pessoaFisica", pessoaFisica).setParameter("tipoPessoa", tipoPessoa).getResultList();
	}
	
	public List<RelPessoaTipo> buscarPorDescricao(String campo, String value, TipoPessoa tipoPessoa) {
		return manager.createQuery("from RelPessoaTipo where " + campo + " like :value and tipoPessoa = :tipoPessoa order by "+ campo, RelPessoaTipo.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").setParameter("tipoPessoa", tipoPessoa).getResultList();
	}

}
