package com.lmsystem.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.AnoLetivo;

public class AnoLetivoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public AnoLetivo obterPorID(Integer pk) {
		return manager.find(AnoLetivo.class, pk);
	}

	public AnoLetivo guardar(AnoLetivo anoLetivo) {
		return manager.merge(anoLetivo);
	}

	public List<AnoLetivo> listarTodos() {
		return manager.createQuery("from AnoLetivo order by anoletivo desc", AnoLetivo.class).getResultList();
	}

	public void remover(AnoLetivo anoLetivo) {
		manager.remove(anoLetivo);
	}

	public List<AnoLetivo> buscarPorDescricao(String campo, String value) {
		return manager
				.createQuery("from AnoLetivo where " + campo + " like :value order by anoLetivo desc", AnoLetivo.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<AnoLetivo> buscarPorRelacionamento(String campo, Object value) {
		return manager
				.createQuery("from AnoLetivo where " + campo + " = :value order by anoLetivo desc", AnoLetivo.class)
				.setParameter("value", value).getResultList();
	}

}
