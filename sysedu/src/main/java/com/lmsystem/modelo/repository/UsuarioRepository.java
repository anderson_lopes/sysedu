package com.lmsystem.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Usuario;

public class UsuarioRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Usuario obterPorID(long pk) {
		return manager.find(Usuario.class, pk);
	}

	public Usuario guardar(Usuario entity) {
		return manager.merge(entity);
	}

	public void remover(Usuario entity) {
		manager.remove(manager.getReference(Usuario.class, entity.getPkusuario()));
	}

	public Usuario buscarporLoginESenha(String login, String senha) {
		return manager.createQuery("from Usuario u where u.login = :login and u.senha = :senha ", Usuario.class)
				.setParameter("login", login).setParameter("senha", senha).getSingleResult();
	}

	public List<Usuario> listarTodos() {
		return manager.createQuery("from Usuario order by pkusuario desc", Usuario.class).getResultList();
	}

	public List<Usuario> buscarPorDescricao(String campo, String value) {
		return manager
				.createQuery("from Usuario where " + campo + " like :value order by pkusuario desc", Usuario.class)
				.setParameter("value", "%" + value + "%").getResultList();
	}

	public List<Usuario> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Usuario where " + campo + " = :value order by pkusuario desc", Usuario.class)
				.setParameter("value", value).getResultList();
	}

}
