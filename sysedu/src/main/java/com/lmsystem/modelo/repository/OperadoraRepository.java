package com.lmsystem.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Operadora;

public class OperadoraRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Operadora obterPorID(Integer pkoperadora) {
		return manager.find(Operadora.class, pkoperadora);
	}

	public Operadora guardar(Operadora entity) {
		return manager.merge(entity);
	}

	public void remover(Operadora entity) {
		manager.remove(manager.getReference(Operadora.class, entity.getPkoperadora()));
	}

	public List<Operadora> listarTodos() {
		return manager.createQuery("from Operadora order by pkoperadora desc", Operadora.class).getResultList();
	}

	public List<Operadora> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from Operadora where " + campo + " like :value order by operadora", Operadora.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<Operadora> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Operadora where " + campo + " = :value order by operadora", Operadora.class)
				.setParameter("value", value).getResultList();
	}

}
