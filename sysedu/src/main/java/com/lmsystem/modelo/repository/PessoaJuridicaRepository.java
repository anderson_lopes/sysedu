package com.lmsystem.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.PessoaJuridica;

public class PessoaJuridicaRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public PessoaJuridica obterPorID(long pk) {
		return manager.find(PessoaJuridica.class, pk);
	}

	public PessoaJuridica guardar(PessoaJuridica entity) {
		return manager.merge(entity);
	}

	public void remover(PessoaJuridica entity) {
		manager.remove(manager.getReference(PessoaJuridica.class, entity.getPkpessoajuridica()));
	}

	public List<PessoaJuridica> listarTodos() {
		return manager.createQuery("from PessoaJuridica order by pkpessoajuridica desc", PessoaJuridica.class)
				.getResultList();
	}

	public List<PessoaJuridica> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from PessoaJuridica where " + campo + " like :value order by pkpessoajuridica desc",
				PessoaJuridica.class).setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<PessoaJuridica> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from PessoaJuridica where " + campo + " = :value order by pkpessoajuridica desc",
				PessoaJuridica.class).setParameter("value", value).getResultList();
	}

}
