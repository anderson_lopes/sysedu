package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tipo_documento")
@NamedQuery(name = "TipoDocumento.findAll", query = "SELECT t FROM TipoDocumento t")
public class TipoDocumento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pktipodocumento;

	@Column(nullable = false, columnDefinition = "text", unique = true)
	private String tipodocumento;

	@Column(columnDefinition = "text", unique = true)
	private String siglatipodocumento;

	@OneToMany(mappedBy = "tipoDocumento", orphanRemoval = true)
	private List<PessoaFisica> pessoaFisicas;

	public TipoDocumento() {
	}

	public Integer getPktipodocumento() {
		return this.pktipodocumento;
	}

	public void setPktipodocumento(Integer pktipodocumento) {
		this.pktipodocumento = pktipodocumento;
	}

	public String getTipodocumento() {
		return this.tipodocumento;
	}

	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}

	public List<PessoaFisica> getPessoaFisicas() {
		return this.pessoaFisicas;
	}

	public void setPessoaFisicas(List<PessoaFisica> pessoaFisicas) {
		this.pessoaFisicas = pessoaFisicas;
	}

	public String getSiglatipodocumento() {
		return siglatipodocumento;
	}

	public void setSiglatipodocumento(String siglatipodocumento) {
		this.siglatipodocumento = siglatipodocumento;
	}

	public PessoaFisica addPessoaFisica(PessoaFisica pessoaFisica) {
		getPessoaFisicas().add(pessoaFisica);
		pessoaFisica.setTipoDocumento(this);

		return pessoaFisica;
	}

	public PessoaFisica removePessoaFisica(PessoaFisica pessoaFisica) {
		getPessoaFisicas().remove(pessoaFisica);
		pessoaFisica.setTipoDocumento(null);

		return pessoaFisica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pktipodocumento == null) ? 0 : pktipodocumento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoDocumento other = (TipoDocumento) obj;
		if (pktipodocumento == null) {
			if (other.pktipodocumento != null)
				return false;
		} else if (!pktipodocumento.equals(other.pktipodocumento))
			return false;
		return true;
	}

}