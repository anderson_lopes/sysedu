package com.lmsystem.modelo;

public enum TipoPessoa {
	ALUNO("Aluno"),
	PROFISSIONAL("Profissional"),
	FUNCIONARIO("Funcionário");

	private String descricao;

	TipoPessoa(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
