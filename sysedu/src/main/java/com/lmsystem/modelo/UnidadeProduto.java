package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "unidade_produto")
@NamedQuery(name = "UnidadeProduto.findAll", query = "SELECT u FROM UnidadeProduto u")
public class UnidadeProduto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer pkunidade;

	@Column(nullable = false)
	private Integer fator;

	@NotEmpty
	@Column(columnDefinition = "text", unique = true)
	private String sigla;

	@NotEmpty
	@Column(columnDefinition = "text", unique = true)
	private String unidade;

	public Integer getPkunidade() {
		return this.pkunidade;
	}

	public void setPkunidade(Integer pkunidade) {
		this.pkunidade = pkunidade;
	}

	public Integer getFator() {
		return this.fator;
	}

	public void setFator(Integer fator) {
		this.fator = fator;
	}

	public String getSigla() {
		return this.sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getUnidade() {
		return this.unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkunidade == null) ? 0 : pkunidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadeProduto other = (UnidadeProduto) obj;
		if (pkunidade == null) {
			if (other.pkunidade != null)
				return false;
		} else if (!pkunidade.equals(other.pkunidade))
			return false;
		return true;
	}

}