package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "raca_cor")
@NamedQuery(name = "RacaCor.findAll", query = "SELECT r FROM RacaCor r")
public class RacaCor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkracacor;

	@Column(nullable = false, columnDefinition = "text")
	private String codracacor;

	@Column(nullable = false, columnDefinition = "text")
	private String racacor;

	@OneToMany(mappedBy = "racaCor", orphanRemoval = true)
	private List<PessoaFisica> pessoaFisicas;

	public RacaCor() {
	}

	public Integer getPkracacor() {
		return this.pkracacor;
	}

	public void setPkracacor(Integer pkracacor) {
		this.pkracacor = pkracacor;
	}

	public String getCodracacor() {
		return this.codracacor;
	}

	public void setCodracacor(String codracacor) {
		this.codracacor = codracacor;
	}

	public String getRacacor() {
		return this.racacor;
	}

	public void setRacacor(String racacor) {
		this.racacor = racacor;
	}

	public List<PessoaFisica> getPessoaFisicas() {
		return this.pessoaFisicas;
	}

	public void setPessoaFisicas(List<PessoaFisica> pessoaFisicas) {
		this.pessoaFisicas = pessoaFisicas;
	}

	public PessoaFisica addPessoaFisica(PessoaFisica pessoaFisica) {
		getPessoaFisicas().add(pessoaFisica);
		pessoaFisica.setRacaCor(this);

		return pessoaFisica;
	}

	public PessoaFisica removePessoaFisica(PessoaFisica pessoaFisica) {
		getPessoaFisicas().remove(pessoaFisica);
		pessoaFisica.setRacaCor(null);

		return pessoaFisica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkracacor == null) ? 0 : pkracacor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RacaCor other = (RacaCor) obj;
		if (pkracacor == null) {
			if (other.pkracacor != null)
				return false;
		} else if (!pkracacor.equals(other.pkracacor))
			return false;
		return true;
	}

}