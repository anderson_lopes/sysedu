package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "ano_letivo")
@NamedQuery(name = "AnoLetivo.findAll", query = "SELECT a FROM AnoLetivo a")
public class AnoLetivo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer pkanoletivo;

	@NotEmpty
	@Column(columnDefinition = "text", unique = true)
	private String anoletivo;

	@Temporal(value = TemporalType.DATE)
	@Column(nullable = false)
	private Date datainicio;

	@Temporal(value = TemporalType.DATE)
	@Column(nullable = false)
	private Date datafim;

	@Column(nullable = false)
	private Integer dias;

	public Integer getPkanoletivo() {
		return pkanoletivo;
	}

	public void setPkanoletivo(Integer pkanoletivo) {
		this.pkanoletivo = pkanoletivo;
	}

	public String getAnoletivo() {
		return anoletivo;
	}

	public void setAnoletivo(String anoletivo) {
		this.anoletivo = anoletivo;
	}

	public Date getDatainicio() {
		return datainicio;
	}

	public void setDatainicio(Date datainicio) {
		this.datainicio = datainicio;
	}

	public Date getDatafim() {
		return datafim;
	}

	public void setDatafim(Date datafim) {
		this.datafim = datafim;
	}

	public Integer getDias() {
		return dias;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkanoletivo == null) ? 0 : pkanoletivo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnoLetivo other = (AnoLetivo) obj;
		if (pkanoletivo == null) {
			if (other.pkanoletivo != null)
				return false;
		} else if (!pkanoletivo.equals(other.pkanoletivo))
			return false;
		return true;
	}

}
