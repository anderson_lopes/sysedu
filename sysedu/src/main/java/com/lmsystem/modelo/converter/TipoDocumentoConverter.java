package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.TipoDocumento;
import com.lmsystem.modelo.repository.TipoDocumentoRepository;

@FacesConverter(forClass = TipoDocumento.class)
public class TipoDocumentoConverter implements Converter {

	@Inject
	protected TipoDocumentoRepository tipoDocumentoRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return tipoDocumentoRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof TipoDocumento) {
			TipoDocumento tipoDocumento = (TipoDocumento) value;
			return String.valueOf(tipoDocumento.getPktipodocumento());
		}
		return "";
	}

}
