package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Municipio;
import com.lmsystem.modelo.repository.MunicipioRepository;

@FacesConverter(forClass = Municipio.class)
public class MunicipioConverter implements Converter {

	@Inject
	protected MunicipioRepository municipioRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return municipioRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Municipio) {
			Municipio municipio = (Municipio) value;
			return String.valueOf(municipio.getPkmunicipio());
		}
		return "";
	}

}
