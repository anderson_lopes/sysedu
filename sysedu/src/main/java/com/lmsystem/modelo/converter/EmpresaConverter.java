package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Empresa;
import com.lmsystem.modelo.repository.EmpresaRepository;

@FacesConverter(forClass = Empresa.class)
public class EmpresaConverter implements Converter {

	@Inject
	protected EmpresaRepository empresaRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return empresaRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Empresa) {
			Empresa empresa = (Empresa) value;
			return String.valueOf(empresa.getPkempresa());
		}
		return "";
	}
}
