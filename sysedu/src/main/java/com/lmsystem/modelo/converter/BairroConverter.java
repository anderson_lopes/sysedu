package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Bairro;
import com.lmsystem.modelo.repository.BairroRepository;

@FacesConverter(forClass = Bairro.class)
public class BairroConverter implements Converter {

	@Inject
	protected BairroRepository bairroRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return bairroRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Bairro) {
			Bairro bairro = (Bairro) value;
			return String.valueOf(bairro.getPkbairro());
		}
		return "";
	}

}
