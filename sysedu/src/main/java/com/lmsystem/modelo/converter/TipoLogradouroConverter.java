package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.TipoLogradouro;
import com.lmsystem.modelo.repository.TipoLogradouroRepository;

@FacesConverter(forClass = TipoLogradouro.class)
public class TipoLogradouroConverter implements Converter {

	@Inject
	protected TipoLogradouroRepository tipoLogradouroRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return tipoLogradouroRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof TipoLogradouro) {
			TipoLogradouro tipoLogradouro = (TipoLogradouro) value;
			return String.valueOf(tipoLogradouro.getPktipologradouro());
		}
		return "";
	}

}
