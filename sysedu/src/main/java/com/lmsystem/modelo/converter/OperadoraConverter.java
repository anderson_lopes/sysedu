package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Operadora;
import com.lmsystem.modelo.repository.OperadoraRepository;

@FacesConverter(forClass = Operadora.class)
public class OperadoraConverter implements Converter {

	@Inject
	private OperadoraRepository operadoraRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return operadoraRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Operadora) {
			Operadora operadora = (Operadora) value;
			return String.valueOf(operadora.getPkoperadora());
		}
		return "";
	}

}
