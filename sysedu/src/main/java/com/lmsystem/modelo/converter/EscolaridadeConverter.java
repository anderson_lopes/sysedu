package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Escolaridade;
import com.lmsystem.modelo.repository.EscolaridadeRepository;

@FacesConverter(forClass = Escolaridade.class)
public class EscolaridadeConverter implements Converter {

	@Inject
	private EscolaridadeRepository escolaridadeRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return escolaridadeRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Escolaridade) {
			Escolaridade escolaridade = (Escolaridade) value;
			return String.valueOf(escolaridade.getPkescolaridade());
		}
		return "";
	}

}
