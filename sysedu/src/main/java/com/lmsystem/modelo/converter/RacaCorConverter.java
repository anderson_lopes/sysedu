package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.RacaCor;
import com.lmsystem.modelo.repository.RacaCorRepository;

@FacesConverter(forClass = RacaCor.class)
public class RacaCorConverter implements Converter {

	@Inject
	RacaCorRepository racaCorRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return racaCorRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof RacaCor) {
			RacaCor racaCor = (RacaCor) value;
			return String.valueOf(racaCor.getPkracacor());
		}
		return "";
	}

}
