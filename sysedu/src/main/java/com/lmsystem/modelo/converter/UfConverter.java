package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Uf;
import com.lmsystem.modelo.repository.UfRepository;

@FacesConverter(forClass = Uf.class)
public class UfConverter implements Converter {

	@Inject
	protected UfRepository ufRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return ufRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Uf) {
			Uf uf = (Uf) value;
			return String.valueOf(uf.getPkuf());
		}
		return "";
	}

}
