package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.EstadoCivil;
import com.lmsystem.modelo.repository.EstadoCivilRepository;

@FacesConverter(forClass = EstadoCivil.class)
public class EstadoCivilConverter implements Converter {

	@Inject
	protected EstadoCivilRepository estadoCivilRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return estadoCivilRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof EstadoCivil) {
			EstadoCivil estadoCivil = (EstadoCivil) value;
			return String.valueOf(estadoCivil.getPkestadocivil());
		}
		return "";
	}

}
