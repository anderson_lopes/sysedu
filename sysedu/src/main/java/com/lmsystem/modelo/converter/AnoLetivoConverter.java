package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.AnoLetivo;
import com.lmsystem.modelo.repository.AnoLetivoRepository;

@FacesConverter(forClass = AnoLetivo.class)
public class AnoLetivoConverter implements Converter {

	@Inject
	protected AnoLetivoRepository anoLetivoRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return anoLetivoRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof AnoLetivo) {
			AnoLetivo anoLetivo = (AnoLetivo) value;
			return String.valueOf(anoLetivo.getPkanoletivo());
		}
		return "";
	}

}
