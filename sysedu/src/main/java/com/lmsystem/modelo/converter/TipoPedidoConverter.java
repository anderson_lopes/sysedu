package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.TipoPedido;
import com.lmsystem.modelo.repository.TipoPedidoRepository;

@FacesConverter(forClass = TipoPedido.class)
public class TipoPedidoConverter implements Converter {

	@Inject
	protected TipoPedidoRepository tipoPedidoRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return tipoPedidoRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof TipoPedido) {
			TipoPedido tipoPedido = (TipoPedido) value;
			return String.valueOf(tipoPedido.getPktipopedido());
		}
		return "";
	}

}
