package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.UnidadeProduto;
import com.lmsystem.modelo.repository.UnidadeProdutoRepository;

@FacesConverter(forClass = UnidadeProduto.class)
public class UnidadeProdutoConverter implements Converter {

	@Inject
	protected UnidadeProdutoRepository unidadeProdutoRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return unidadeProdutoRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof UnidadeProduto) {
			UnidadeProduto unidadeProduto = (UnidadeProduto) value;
			return String.valueOf(unidadeProduto.getPkunidade());
		}
		return "";
	}

}
