package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Etnia;
import com.lmsystem.modelo.repository.EtniaRepository;

@FacesConverter(forClass = Etnia.class)
public class EtniaConverter implements Converter {

	@Inject
	protected EtniaRepository etniaRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return etniaRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Etnia) {
			Etnia etnia = (Etnia) value;
			return String.valueOf(etnia.getPketnia());
		}
		return "";
	}

}
