package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Ocupacao;
import com.lmsystem.modelo.repository.OcupacaoRepository;

@FacesConverter(forClass = Ocupacao.class)
public class OcupacaoConverter implements Converter {

	@Inject
	protected OcupacaoRepository ocupacaoRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return ocupacaoRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Ocupacao) {
			Ocupacao ocupacao = (Ocupacao) value;
			return String.valueOf(ocupacao.getPkocupacao());
		}
		return "";
	}

}
