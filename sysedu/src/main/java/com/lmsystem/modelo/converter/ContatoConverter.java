package com.lmsystem.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Contato;
import com.lmsystem.modelo.repository.ContatoRepository;

@FacesConverter(forClass = Contato.class)
public class ContatoConverter implements Converter {

	@Inject
	protected ContatoRepository contatoRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return contatoRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Contato) {
			Contato contato = (Contato) value;
			return String.valueOf(contato.getPkcontato());
		}
		return "";
	}
}
