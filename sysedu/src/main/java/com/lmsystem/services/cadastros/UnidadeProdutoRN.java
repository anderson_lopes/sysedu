package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.UnidadeProduto;
import com.lmsystem.modelo.repository.UnidadeProdutoRepository;
import com.lmsystem.util.Transacional;

public class UnidadeProdutoRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UnidadeProdutoRepository unidadeProdutoRepository;

	@Transacional
	public void salvar(UnidadeProduto unidadeProduto) {
		unidadeProdutoRepository.guardar(unidadeProduto);
	}

	@Transacional
	public void remover(UnidadeProduto unidadeProduto) {
		unidadeProdutoRepository.remover(unidadeProduto);
	}

	@Transacional
	public List<UnidadeProduto> listarTodos(String nomeunidade) {
		if (!nomeunidade.trim().equals("")) {
			return unidadeProdutoRepository.buscarPorDescricao("unidade", nomeunidade);
		}
		return unidadeProdutoRepository.listarTodos();
	}

}
