package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Pais;
import com.lmsystem.modelo.Uf;
import com.lmsystem.modelo.repository.PaisRepository;
import com.lmsystem.modelo.repository.UfRepository;
import com.lmsystem.util.Transacional;

public class UfRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected UfRepository ufRepository;

	@Inject
	protected PaisRepository paisRepository;

	@Transacional
	public void salvar(Uf uf) {
		ufRepository.guardar(uf);
	}

	@Transacional
	public void remover(Uf uf) {
		ufRepository.remover(uf);
	}

	@Transacional
	public List<Uf> listarTodos(String nomeUF) {
		if (!nomeUF.trim().equals("")) {
			return ufRepository.buscarPorDescricao("uf", nomeUF);
		}
		return ufRepository.listarTodos();
	}

	@Transacional
	public List<Pais> listarPaises(String nomePais) {
		if (!nomePais.trim().equals("")) {
			return paisRepository.buscarPorDescricao("pais", nomePais);
		}
		return paisRepository.listarTodos();
	}

}
