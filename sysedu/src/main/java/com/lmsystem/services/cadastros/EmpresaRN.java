package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Empresa;
import com.lmsystem.modelo.Uf;
import com.lmsystem.modelo.repository.EmpresaRepository;
import com.lmsystem.modelo.repository.UfRepository;
import com.lmsystem.util.Transacional;

public class EmpresaRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected EmpresaRepository empresaRepository;

	@Inject
	protected UfRepository ufRepository;

	@Transacional
	public void salvar(Empresa empresa) {
		empresaRepository.guardar(empresa);
	}

	@Transacional
	public void remover(Empresa empresa) {
		empresaRepository.remover(empresa);
	}

	@Transacional
	public List<Empresa> listarTodos(String nomeEmpresa) {
		if (!nomeEmpresa.trim().equals("")) {
			return empresaRepository.buscarPorDescricao("empresa", nomeEmpresa);
		}
		return empresaRepository.listarTodos();
	}

	@Transacional
	public List<Uf> listarUfs(String uf) {
		if (!uf.trim().equals("")) {
			return ufRepository.buscarPorDescricao("uf", uf);
		}
		return ufRepository.listarTodos();
	}

}
