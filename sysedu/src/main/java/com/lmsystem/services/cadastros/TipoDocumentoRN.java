package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.TipoDocumento;
import com.lmsystem.modelo.repository.TipoDocumentoRepository;
import com.lmsystem.util.Transacional;

public class TipoDocumentoRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected TipoDocumentoRepository tipoDocumentoRepository;

	@Transacional
	public void salvar(TipoDocumento tipoDocumento) {
		tipoDocumentoRepository.guardar(tipoDocumento);
	}

	@Transacional
	public void remover(TipoDocumento tipoDocumento) {
		tipoDocumentoRepository.remover(tipoDocumento);
	}

	@Transacional
	public List<TipoDocumento> listarTodos(String nomeTipoDocumento) {
		if (!nomeTipoDocumento.trim().equals("")) {
			return tipoDocumentoRepository.buscarPorDescricao("tipodocumento", nomeTipoDocumento);
		}
		return tipoDocumentoRepository.listarTodos();
	}

	@Transacional
	public List<TipoDocumento> listarTipoDocumentos(String nomeTipoDocumento) {
		if (!nomeTipoDocumento.trim().equals("")) {
			return tipoDocumentoRepository.buscarPorDescricao("tipodocumento", nomeTipoDocumento);
		}
		return tipoDocumentoRepository.listarTodos();
	}

}
