package com.lmsystem.services.cadastros;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Bairro;
import com.lmsystem.modelo.Etnia;
import com.lmsystem.modelo.Municipio;
import com.lmsystem.modelo.Nacionalidade;
import com.lmsystem.modelo.RacaCor;
import com.lmsystem.modelo.RelPessoaTipo;
import com.lmsystem.modelo.Sexo;
import com.lmsystem.modelo.TipoLogradouro;
import com.lmsystem.modelo.TipoPessoa;
import com.lmsystem.modelo.repository.BairroRepository;
import com.lmsystem.modelo.repository.ContatoRepository;
import com.lmsystem.modelo.repository.EtniaRepository;
import com.lmsystem.modelo.repository.MunicipioRepository;
import com.lmsystem.modelo.repository.NacionalidadeRepository;
import com.lmsystem.modelo.repository.RacaCorRepository;
import com.lmsystem.modelo.repository.RelPessoaTipoRepository;
import com.lmsystem.modelo.repository.SexoRepository;
import com.lmsystem.modelo.repository.TipoLogradouroRepository;
import com.lmsystem.util.BuscaCEP;
import com.lmsystem.util.Transacional;

public class AlunoRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected ContatoRepository contatoRepository;

	@Inject
	protected SexoRepository sexoRepository;

	@Inject
	protected RacaCorRepository racaCorRepository;

	@Inject
	protected EtniaRepository etniaRepository;

	@Inject
	protected NacionalidadeRepository nacionalidadeRepository;

	@Inject
	protected MunicipioRepository municipioRepository;

	@Inject
	protected TipoLogradouroRepository tipoLogradouroRepository;

	@Inject
	protected BairroRepository bairroRepository;

	@Inject
	protected RelPessoaTipoRepository relPessoaTipoRepository;

	@Transacional
	public void salvar(RelPessoaTipo relPessoaTipo) {
		if (relPessoaTipo.getPessoaFisica().getCpf().trim().equals("")) {
			relPessoaTipo.getPessoaFisica().setCpf(null);
		}
		relPessoaTipo.setTipoPessoa(TipoPessoa.ALUNO);
		relPessoaTipoRepository.guardar(relPessoaTipo);
	}

	@Transacional
	public void remover(RelPessoaTipo relPessoaTipo) {
		relPessoaTipoRepository.remover(relPessoaTipo);
	}

	@Transacional
	public List<RelPessoaTipo> listarTodos(String nomePessoa) {
		if (!nomePessoa.trim().equals("")) {
			return relPessoaTipoRepository.buscarPorDescricao("pessoaFisica.nome",nomePessoa, TipoPessoa.ALUNO);
		}
		return relPessoaTipoRepository.listarTodos(TipoPessoa.ALUNO);
	}

	public List<Sexo> listarSexos(String nomeSexo) {
		if (!nomeSexo.trim().equals("")) {
			return sexoRepository.buscarPorDescricao("sexo", nomeSexo);
		}
		return sexoRepository.listarTodos();
	}

	public List<RacaCor> listarRacasCor(String nomeRacaCor) {
		if (!nomeRacaCor.trim().equals("")) {
			return racaCorRepository.buscarPorDescricao("racacor", nomeRacaCor);
		}
		return racaCorRepository.listarTodos();
	}

	public List<Nacionalidade> ListarNacionalidades(String nomeNacionalidade) {
		if (!nomeNacionalidade.trim().equals("")) {
			return nacionalidadeRepository.buscarPorDescricao("nacionalidade", nomeNacionalidade);
		}
		return nacionalidadeRepository.listarTodos();
	}

	public List<Etnia> listarEtnias(String nomeEtnia) {
		if (!nomeEtnia.trim().equals("")) {
			return etniaRepository.buscarPorDescricao("etnia", nomeEtnia);
		}
		return etniaRepository.listarTodos();
	}

	public List<Municipio> listarMunicipios(String nomemunicipio) {
		if (!nomemunicipio.trim().equals("")) {
			return municipioRepository.buscarPorDescricao("municipio", nomemunicipio);
		}
		return municipioRepository.listarTodos();
	}

	public List<TipoLogradouro> listarTipoLogradouros(String nomeTipoLogradouro) {
		if (!nomeTipoLogradouro.trim().equals("")) {
			return tipoLogradouroRepository.buscarPorDescricao("tipologradouro", nomeTipoLogradouro);
		}
		return tipoLogradouroRepository.listarTodos();
	}

	public List<Bairro> listarBairros(String nomeBairro) {
		if (!nomeBairro.trim().equals("")) {
			return bairroRepository.buscarPorDescricao("bairro", nomeBairro);
		}
		return bairroRepository.listarTodos();
	}

	public RelPessoaTipo buscarCEP(RelPessoaTipo relPessoaTipo) {
		BuscaCEP cep = new BuscaCEP();
		try {
			if (!relPessoaTipo.getPessoaFisica().getEndereco().getCep().trim().equals("")
					|| relPessoaTipo.getPessoaFisica().getEndereco().getCep() != null) {
				String tmp = cep.getCidade(relPessoaTipo.getPessoaFisica().getEndereco().getCep());
				if (tmp != null) {
					relPessoaTipo.getPessoaFisica().getEndereco()
							.setMunicipio(municipioRepository.buscarPorDescricao("municipio", tmp).get(0));
					tmp = null;
				}
				tmp = cep.getEndereco(relPessoaTipo.getPessoaFisica().getEndereco().getCep());
				if (tmp != null) {
					relPessoaTipo.getPessoaFisica().getEndereco().setEndereco(tmp);
					tmp = null;
				}
				tmp = cep.getBairro(relPessoaTipo.getPessoaFisica().getEndereco().getCep());
				if (tmp != null) {
					System.out.println(tmp);
					relPessoaTipo.getPessoaFisica().getEndereco()
							.setBairro(bairroRepository.buscarPorDescricao("bairro", tmp).get(0));
					tmp = null;
				}
			}
			return relPessoaTipo;
		} catch (IOException e) {
			System.out.println("Endereço não encontrado!");
			e.printStackTrace();
			return relPessoaTipo;
		}
	}

}
