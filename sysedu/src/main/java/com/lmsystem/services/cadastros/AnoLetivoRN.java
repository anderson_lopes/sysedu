package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.AnoLetivo;
import com.lmsystem.modelo.repository.AnoLetivoRepository;
import com.lmsystem.util.Transacional;

public class AnoLetivoRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected AnoLetivoRepository anoLetivoRepository;

	@Transacional
	public void salvar(AnoLetivo anoLetivo) {
		anoLetivoRepository.guardar(anoLetivo);
	}

	@Transacional
	public void remover(AnoLetivo anoLetivo) {
		anoLetivoRepository.remover(anoLetivo);
	}

	@Transacional
	public List<AnoLetivo> listarTodos(String nomeAnoLetivo) {
		if (!nomeAnoLetivo.trim().equals("")) {
			return anoLetivoRepository.buscarPorDescricao("anoletivo", nomeAnoLetivo);
		}
		return anoLetivoRepository.listarTodos();
	}

}
