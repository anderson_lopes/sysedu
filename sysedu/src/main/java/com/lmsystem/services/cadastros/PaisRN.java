package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Pais;
import com.lmsystem.modelo.repository.PaisRepository;
import com.lmsystem.util.Transacional;

public class PaisRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected PaisRepository paisRepository;

	@Transacional
	public void salvar(Pais pais) {
		paisRepository.guardar(pais);
	}

	@Transacional
	public void remover(Pais pais) {
		paisRepository.remover(pais);
	}

	@Transacional
	public List<Pais> listarTodos(String nomePais) {
		if (!nomePais.trim().equals("")) {
			return paisRepository.buscarPorDescricao("pais", nomePais);
		}
		return paisRepository.listarTodos();
	}

}
