package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Escolaridade;
import com.lmsystem.modelo.repository.EscolaridadeRepository;
import com.lmsystem.util.Transacional;

public class EscolaridadeRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected EscolaridadeRepository escolaridadeRepository;

	@Transacional
	public void salvar(Escolaridade escolaridade) {
		escolaridadeRepository.guardar(escolaridade);
	}

	@Transacional
	public void remover(Escolaridade escolaridade) {
		escolaridadeRepository.remover(escolaridade);
	}

	@Transacional
	public List<Escolaridade> listarTodos(String nomeEscolaridade) {
		if (!nomeEscolaridade.trim().equals("")) {
			return escolaridadeRepository.buscarPorDescricao("escolaridade", nomeEscolaridade);
		}
		return escolaridadeRepository.listarTodos();
	}

}
