package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.RacaCor;
import com.lmsystem.modelo.repository.RacaCorRepository;
import com.lmsystem.util.Transacional;

public class RacaCorRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected RacaCorRepository racaCorRepository;

	@Transacional
	public void salvar(RacaCor racaCor) {
		racaCorRepository.guardar(racaCor);
	}

	@Transacional
	public void remover(RacaCor racaCor) {
		racaCorRepository.remover(racaCor);
	}

	@Transacional
	public List<RacaCor> listarTodos(String nomeRacaCor) {
		if (!nomeRacaCor.trim().equals("")) {
			return racaCorRepository.buscarPorDescricao("racacor", nomeRacaCor);
		}
		return racaCorRepository.listarTodos();
	}

}
