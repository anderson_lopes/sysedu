package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Disciplina;
import com.lmsystem.modelo.repository.DisciplinaRepository;
import com.lmsystem.util.Transacional;

public class DisciplinaRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected DisciplinaRepository disciplinaRepository;

	@Transacional
	public void salvar(Disciplina disciplina) {
		disciplinaRepository.guardar(disciplina);
	}

	@Transacional
	public void remover(Disciplina disciplina) {
		disciplinaRepository.remover(disciplina);
	}

	@Transacional
	public List<Disciplina> listarTodos(String nomeDisciplina) {
		if (!nomeDisciplina.trim().equals("")) {
			return disciplinaRepository.buscarPorDescricao("disciplina", nomeDisciplina);
		}
		return disciplinaRepository.listarTodos();
	}

}
