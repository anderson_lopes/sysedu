package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Deposito;
import com.lmsystem.modelo.repository.DepositoRepository;
import com.lmsystem.util.Transacional;

public class DepositoRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected DepositoRepository depositoRepository;

	@Transacional
	public void salvar(Deposito deposito) {
		depositoRepository.guardar(deposito);
	}

	@Transacional
	public void remover(Deposito deposito) {
		depositoRepository.remover(deposito);
	}

	@Transacional
	public List<Deposito> listarTodos(String nomeDeposito) {
		if (!nomeDeposito.trim().equals("")) {
			return depositoRepository.buscarPorDescricao("deposito", nomeDeposito);
		}
		return depositoRepository.listarTodos();
	}

}
