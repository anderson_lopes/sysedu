package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.TipoLogradouro;
import com.lmsystem.modelo.repository.TipoLogradouroRepository;
import com.lmsystem.util.Transacional;

public class TipoLogradouroRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected TipoLogradouroRepository tipoLogradouroRepository;

	@Transacional
	public void salvar(TipoLogradouro tipoLogradouro) {
		tipoLogradouroRepository.guardar(tipoLogradouro);
	}

	@Transacional
	public void remover(TipoLogradouro tipoLogradouro) {
		tipoLogradouroRepository.remover(tipoLogradouro);
	}

	@Transacional
	public List<TipoLogradouro> listarTodos(String nomeTipoLogradouro) {
		if (!nomeTipoLogradouro.trim().equals("")) {
			return tipoLogradouroRepository.buscarPorDescricao("tipologradouro", nomeTipoLogradouro);
		}
		return tipoLogradouroRepository.listarTodos();
	}

	@Transacional
	public List<TipoLogradouro> listarTipoLogradouros(String nomeTipoLogradouro) {
		if (!nomeTipoLogradouro.trim().equals("")) {
			return tipoLogradouroRepository.buscarPorDescricao("tipologradouro", nomeTipoLogradouro);
		}
		return tipoLogradouroRepository.listarTodos();
	}

}
