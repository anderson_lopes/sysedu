package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Produto;
import com.lmsystem.modelo.repository.ProdutoRepository;
import com.lmsystem.util.Transacional;

public class ProdutoRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected ProdutoRepository produtoRepository;

	@Transacional
	public void salvar(Produto produto) {
		produtoRepository.guardar(produto);
	}

	@Transacional
	public void remover(Produto produto) {
		produtoRepository.remover(produto);
	}

	@Transacional
	public List<Produto> listarTodos(String nomeProduto) {
		if (!nomeProduto.trim().equals("")) {
			return produtoRepository.buscarPorDescricao("produto", nomeProduto);
		}
		return produtoRepository.listarTodos();
	}

}
