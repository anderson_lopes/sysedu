package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Banco;
import com.lmsystem.modelo.repository.BancoRepository;
import com.lmsystem.util.Transacional;

public class BancoRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected BancoRepository bancoRepository;

	@Transacional
	public void salvar(Banco banco) {
		bancoRepository.guardar(banco);
	}

	@Transacional
	public void remover(Banco banco) {
		bancoRepository.remover(banco);
	}

	@Transacional
	public List<Banco> listarTodos(String nomeBanco) {
		if (!nomeBanco.trim().equals("")) {
			return bancoRepository.buscarPorDescricao("banco", nomeBanco);
		}
		return bancoRepository.listarTodos();
	}

}
