package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Usuario;
import com.lmsystem.modelo.repository.UsuarioRepository;
import com.lmsystem.util.Transacional;
import com.lmsystem.util.Utils;

public class UsuarioRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected UsuarioRepository usuarioRepository;

	@Transacional
	public void salvar(Usuario usuario) {
		usuario.setSenha(Utils.transformaEmMD5(usuario.getSenha()));
		usuarioRepository.guardar(usuario);
	}

	@Transacional
	public void remover(Usuario usuario) {
		usuarioRepository.remover(usuario);
	}

	@Transacional
	public List<Usuario> listarTodos(String nomeUsuario) {
		if (!nomeUsuario.trim().equals("")) {
			return usuarioRepository.buscarPorDescricao("nome", nomeUsuario);
		}
		return usuarioRepository.listarTodos();
	}

	@Transacional
	public Usuario buscarUsuario(String login, String senha) {
		senha = Utils.transformaEmMD5(senha);
		return usuarioRepository.buscarporLoginESenha(login, senha);
	}

}
