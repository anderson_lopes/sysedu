package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.TipoPedido;
import com.lmsystem.services.cadastros.TipoPedidoRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class TipoPedidoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private TipoPedido tipoPedido;

	private String nomeTipoPedido;

	private List<TipoPedido> tipoPedidos;

	@Inject
	protected TipoPedidoRN tipoPedidoRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeTipoPedido("");
		pesquisar();
	}

	public void novo() {
		this.tipoPedido = new TipoPedido();
	}

	public void salvar() {
		try {
			this.tipoPedidoRN.salvar(this.tipoPedido);
			this.tipoPedido = new TipoPedido();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.tipoPedido = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
		}
	}

	public void remover() {
		try {
			tipoPedidoRN.remover(this.tipoPedido);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.tipoPedido = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
		}
	}

	public void limpar() {
		this.tipoPedido = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/estoque/tipo-pedido", opcoes, null);
	}

	public void pesquisar() {
		this.getTipoPedidos();
	}

	public TipoPedido getTipoPedido() {
		return tipoPedido;
	}

	public void setTipoPedido(TipoPedido tipoPedido) {
		this.tipoPedido = tipoPedido;
	}

	public List<TipoPedido> getTipoPedidos() {
		this.tipoPedidos = tipoPedidoRN.listarTodos(getNomeTipoPedido());
		return tipoPedidos;
	}

	public String getNomeTipoPedido() {
		return nomeTipoPedido;
	}

	public void setNomeTipoPedido(String nomeTipoPedido) {
		this.nomeTipoPedido = nomeTipoPedido;
	}

}
