package com.lmsystem.controller.cadastros;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Bairro;
import com.lmsystem.modelo.Contato;
import com.lmsystem.modelo.Endereco;
import com.lmsystem.modelo.Municipio;
import com.lmsystem.modelo.PessoaJuridica;
import com.lmsystem.modelo.TipoLogradouro;
import com.lmsystem.modelo.repository.BairroRepository;
import com.lmsystem.modelo.repository.MunicipioRepository;
import com.lmsystem.services.cadastros.PessoaJuridicaRN;
import com.lmsystem.util.BuscaCEP;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class PessoaJuridicaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private PessoaJuridica pessoaJuridica;

	private String nomefantasia;

	private List<PessoaJuridica> pessoasJuridica;

	@Inject
	protected PessoaJuridicaRN pessoaRN;

	@Inject
	protected BairroRepository bairroRepository;

	@Inject
	protected MunicipioRepository municipioRepository;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomefantasia("");
		pesquisar();
	}

	public void novo() {
		this.pessoaJuridica = new PessoaJuridica();
		this.getPessoaJuridica().setContato(new Contato());
		this.getPessoaJuridica().setEndereco(new Endereco());
	}

	public void salvar() {
		try {
			pessoaRN.salvar(getPessoaJuridica());
			pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.pessoaJuridica = null;
			iniciar();
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
		}
	}

	public void remover() {
		try {
			pessoaRN.remover(this.getPessoaJuridica());
			this.pesquisar();
			facesUtils.exibeSucesso("registro removido com sucesso!", null);
			this.pessoaJuridica = null;
			iniciar();
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
		}
	}

	public void limpar() {
		this.pessoaJuridica = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		opcoes.put("resizable", false);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/pessoajuridica", opcoes, null);
	}

	public void pesquisar() {
		this.getPessoasFisicas();
	}

	public void buscarCEP() {
		BuscaCEP cep = new BuscaCEP();
		try {
			if (!this.getPessoaJuridica().getEndereco().getCep().trim().equals("")
					|| this.getPessoaJuridica().getEndereco().getCep() != null) {
				String tmp = cep.getCidade(this.getPessoaJuridica().getEndereco().getCep());
				if (tmp != null) {
					this.getPessoaJuridica().getEndereco()
							.setMunicipio(municipioRepository.buscarPorDescricao("municipio", tmp).get(0));
					tmp = null;
				}
				tmp = cep.getEndereco(this.getPessoaJuridica().getEndereco().getCep());
				if (tmp != null) {
					this.getPessoaJuridica().getEndereco().setEndereco(tmp);
					tmp = null;
				}
				tmp = cep.getBairro(this.getPessoaJuridica().getEndereco().getCep());
				if (tmp != null) {
					System.out.println(tmp);
					this.getPessoaJuridica().getEndereco()
							.setBairro(bairroRepository.buscarPorDescricao("bairro", tmp).get(0));
					tmp = null;
				}
			}
		} catch (IOException e) {
			System.out.println("Endereço não encontrado!");
			e.printStackTrace();
		}
	}

	private List<PessoaJuridica> getPessoasFisicas() {
		pessoasJuridica = pessoaRN.listarTodos(getNomefantasia());
		return pessoasJuridica;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	public String getNomefantasia() {
		return nomefantasia;
	}

	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}

	public List<Municipio> listarMunicipios(String query) {
		return pessoaRN.listarMunicipios(query);
	}

	public List<TipoLogradouro> listarTiposLogradouro(String query) {
		return pessoaRN.listarTipoLogradouros(query);
	}

	public List<Bairro> listarBairros(String query) {
		return pessoaRN.listarBairros(query);
	}

	public List<PessoaJuridica> getPessoasJuridica() {
		this.pessoasJuridica = pessoaRN.listarTodos(getNomefantasia());
		return pessoasJuridica;
	}
}
