package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Etnia;
import com.lmsystem.services.cadastros.EtniaRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class EtniaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Etnia etnia;

	private String nomeEtnia;

	private List<Etnia> etnias;

	@Inject
	protected EtniaRN etniaRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeEtnia("");
		pesquisar();
	}

	public void novo() {
		this.etnia = new Etnia();
	}

	public void salvar() {
		try {
			etniaRN.salvar(this.etnia);
			this.etnia = new Etnia();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.etnia = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			etniaRN.remover(this.etnia);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.etnia = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void limpar() {
		this.etnia = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/etnia", opcoes, null);
	}

	public void pesquisar() {
		this.getEtnias();
	}

	public Etnia getEtnia() {
		return etnia;
	}

	public void setEtnia(Etnia etnia) {
		this.etnia = etnia;
	}

	public String getNomeEtnia() {
		return nomeEtnia;
	}

	public void setNomeEtnia(String nomeEtnia) {
		this.nomeEtnia = nomeEtnia;
	}

	public List<Etnia> getEtnias() {
		this.etnias = etniaRN.listarTodos(getNomeEtnia());
		return etnias;
	}

}
