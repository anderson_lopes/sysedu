package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Disciplina;
import com.lmsystem.services.cadastros.DisciplinaRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class DisciplinaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Disciplina disciplina;

	private String nomeDisciplina;

	private List<Disciplina> disciplinas;

	@Inject
	protected DisciplinaRN disciplinaRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeDisciplina("");
		pesquisar();
	}

	public void novo() {
		this.disciplina = new Disciplina();
	}

	public void salvar() {
		try {
			disciplinaRN.salvar(this.disciplina);
			this.disciplina = new Disciplina();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.disciplina = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
		}
	}

	public void remover() {
		try {
			disciplinaRN.remover(this.disciplina);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.disciplina = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
		}
	}

	public void limpar() {
		this.disciplina = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/academico/disciplina", opcoes, null);
	}

	public void pesquisar() {
		this.getDisciplinas();
	}

	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}

	public String getNomeDisciplina() {
		return nomeDisciplina;
	}

	public void setNomeDisciplina(String nomeDisciplina) {
		this.nomeDisciplina = nomeDisciplina;
	}

	public List<Disciplina> getDisciplinas() {
		this.disciplinas = disciplinaRN.listarTodos(getNomeDisciplina());
		return disciplinas;
	}

}
