package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.EstadoCivil;
import com.lmsystem.services.cadastros.EstadoCivilRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class EstadoCivilBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private EstadoCivil estadoCivil;;

	private String nomeEstadoCivil;

	private List<EstadoCivil> estadosCivis;

	@Inject
	protected EstadoCivilRN estadoCivilRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeEstadoCivil("");
		pesquisar();
	}

	public void novo() {
		this.estadoCivil = new EstadoCivil();
	}

	public void salvar() {
		try {
			estadoCivilRN.salvar(this.estadoCivil);
			this.estadoCivil = new EstadoCivil();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.estadoCivil = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			estadoCivilRN.remover(this.estadoCivil);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.estadoCivil = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void limpar() {
		this.estadoCivil = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/estado-civil", opcoes, null);
	}

	public void pesquisar() {
		this.getEstadosCivis();
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getNomeEstadoCivil() {
		return nomeEstadoCivil;
	}

	public void setNomeEstadoCivil(String nomeEstadoCivil) {
		this.nomeEstadoCivil = nomeEstadoCivil;
	}

	public List<EstadoCivil> getEstadosCivis() {
		this.estadosCivis = estadoCivilRN.listarTodos(getNomeEstadoCivil());
		return estadosCivis;
	}

}
