package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.AnoLetivo;
import com.lmsystem.services.cadastros.AnoLetivoRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class AnoLetivoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private AnoLetivo anoLetivo;

	private String nomeAnoLetivo;
	
	private List<AnoLetivo> anosLetivos;

	@Inject
	protected AnoLetivoRN anoLetivoRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeAnoLetivo("");
		pesquisar();
	}

	public void novo() {
		this.anoLetivo = new AnoLetivo();
	}

	public void salvar() {
		try {
			anoLetivoRN.salvar(this.anoLetivo);
			this.anoLetivo = new AnoLetivo();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.anoLetivo = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
		}
	}

	public void remover() {
		try {
			anoLetivoRN.remover(this.anoLetivo);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.anoLetivo = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
		}
	}

	public void limpar() {
		this.anoLetivo = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		//opcoes.put("modal", true);
		//opcoes.put("draggable", false);
		opcoes.put("resizable", false);
		opcoes.put("contentWidth", 1024);
		opcoes.put("contentHeight", 768);

		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/academico/anoletivo", opcoes, null);
	}

	public void pesquisar() {
		this.getAnosLetivos();
	}

	public AnoLetivo getAnoLetivo() {
		return anoLetivo;
	}

	public void setAnoLetivo(AnoLetivo anoLetivo) {
		this.anoLetivo = anoLetivo;
	}

	public String getNomeAnoLetivo() {
		return nomeAnoLetivo;
	}

	public void setNomeAnoLetivo(String nomeAnoLetivo) {
		this.nomeAnoLetivo = nomeAnoLetivo;
	}

	public List<AnoLetivo> getAnosLetivos() {
		this.anosLetivos = anoLetivoRN.listarTodos(getNomeAnoLetivo());
		return anosLetivos;
	}

}
