package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Pais;
import com.lmsystem.modelo.Uf;
import com.lmsystem.services.cadastros.UfRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class UfBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Uf uf;

	private String nomeUF;

	private List<Uf> ufs;

	@Inject
	protected UfRN ufrn;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeUF("");
		pesquisar();
	}

	public void novo() {
		this.uf = new Uf();
	}

	public void salvar() {
		try {
			ufrn.salvar(this.uf);
			this.uf = new Uf();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.uf = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
		}
	}

	public void remover() {
		try {
			ufrn.remover(this.uf);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.uf = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
		}
	}

	public void limpar() {
		this.uf = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/uf", opcoes, null);
	}

	public void pesquisar() {
		this.getUfs();
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public String getNomeUF() {
		return nomeUF;
	}

	public void setNomeUF(String nomeUF) {
		this.nomeUF = nomeUF;
	}

	public List<Uf> getUfs() {
		this.ufs = ufrn.listarTodos(getNomeUF());
		return ufs;
	}

	public List<Pais> listarPaises(String query) {
		return ufrn.listarPaises(query);
	}

}
