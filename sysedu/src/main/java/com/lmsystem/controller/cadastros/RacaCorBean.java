package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.RacaCor;
import com.lmsystem.services.cadastros.RacaCorRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class RacaCorBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private RacaCor racaCor;

	private String nomeRacaCor;

	private List<RacaCor> racasCor;

	@Inject
	protected RacaCorRN racaCorRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeRacaCor("");
		pesquisar();
	}

	public void novo() {
		this.racaCor = new RacaCor();
	}

	public void salvar() {
		try {
			racaCorRN.salvar(this.getRacaCor());
			this.setRacaCor(new RacaCor());
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.setRacaCor(null);
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
		}
	}

	public void remover() {
		try {
			racaCorRN.remover(this.getRacaCor());
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.setRacaCor(null);
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
		}
	}

	public void limpar() {
		this.setRacaCor(null);
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/raca-cor", opcoes, null);
	}

	public void pesquisar() {
		this.getRacasCor();
	}

	public String getNomeRacaCor() {
		return nomeRacaCor;
	}

	public void setNomeRacaCor(String nomeRacaCor) {
		this.nomeRacaCor = nomeRacaCor;
	}

	public RacaCor getRacaCor() {
		return racaCor;
	}

	public void setRacaCor(RacaCor racaCor) {
		this.racaCor = racaCor;
	}

	public List<RacaCor> getRacasCor() {
		this.racasCor = racaCorRN.listarTodos(getNomeRacaCor());
		return racasCor;
	}

}
