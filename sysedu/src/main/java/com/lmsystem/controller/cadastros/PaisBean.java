package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Pais;
import com.lmsystem.services.cadastros.PaisRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class PaisBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Pais pais;

	private String nomePais;

	private List<Pais> paises;

	@Inject
	protected PaisRN paisRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomePais("");
		pesquisar();
	}

	public void novo() {
		this.pais = new Pais();
	}

	public void salvar() {
		try {
			paisRN.salvar(this.pais);
			this.pais = new Pais();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.pais = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
		}
	}

	public void remover() {
		try {
			paisRN.remover(this.pais);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.pais = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
		}
	}

	public void limpar() {
		this.pais = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/pais", opcoes, null);
	}

	public void pesquisar() {
		this.getPaises();
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public String getNomePais() {
		return nomePais;
	}

	public void setNomePais(String nomePais) {
		this.nomePais = nomePais;
	}

	public List<Pais> getPaises() {
		this.paises = paisRN.listarTodos(getNomePais());
		return paises;
	}

}
