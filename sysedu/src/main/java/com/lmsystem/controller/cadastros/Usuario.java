package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.lmsystem.modelo.Permissoes;

@Named
@SessionScoped
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	private String locale = "pt";
	private String encoding = "iso-8859-1";
	private String timeZone = "America/Fortaleza";

	private String nome;
	private Date dataLogin;
	private String funcao;

	public boolean isLogado() {
		return nome != null;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataLogin() {
		return dataLogin;
	}

	public void setDataLogin(Date dataLogin) {
		this.dataLogin = dataLogin;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public boolean podeVerGraficos() {
		return Permissoes.ADMINISTRADOR.name().equals(funcao) || Permissoes.GERENTE.name().equals(funcao);
	}

	public boolean podeAdministrar() {
		if (nome.equals("lopes")) {
			return true;
		}
		return Permissoes.ADMINISTRADOR.name().equals(funcao);
	}

	public String getFuncao() {
		return funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

}
