package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Banco;
import com.lmsystem.services.cadastros.BancoRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class BancoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Banco banco;

	private String nomeBanco;

	private List<Banco> bancos;

	@Inject
	protected BancoRN bancoRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeBanco("");
		pesquisar();
	}

	public void novo() {
		this.banco = new Banco();
	}

	public void salvar() {
		try {
			bancoRN.salvar(this.banco);
			this.banco = new Banco();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.banco = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			bancoRN.remover(this.banco);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.banco = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void limpar() {
		this.banco = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/banco", opcoes, null);
	}

	public void pesquisar() {
		this.getBancos();
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public String getNomeBanco() {
		return nomeBanco;
	}

	public void setNomeBanco(String nomeBanco) {
		this.nomeBanco = nomeBanco;
	}

	public List<Banco> getBancos() {
		this.bancos = bancoRN.listarTodos(getNomeBanco());
		return bancos;
	}

}
