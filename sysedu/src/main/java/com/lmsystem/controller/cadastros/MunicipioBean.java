package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Municipio;
import com.lmsystem.modelo.Uf;
import com.lmsystem.services.cadastros.MunicipioRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class MunicipioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Municipio municipio;

	private String nomeMunicipio;

	private List<Municipio> municipios;

	@Inject
	protected MunicipioRN municipioRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeMunicipio("");
		pesquisar();
	}

	public void novo() {
		this.municipio = new Municipio();
	}

	public void salvar() {
		try {
			municipioRN.salvar(this.municipio);
			this.municipio = new Municipio();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.municipio = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			municipioRN.remover(this.municipio);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.municipio = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void limpar() {
		this.municipio = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/municipio", opcoes, null);
	}

	public void pesquisar() {
		this.getMunicipios();
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getNomeMunicipio() {
		return nomeMunicipio;
	}

	public void setNomeMunicipio(String nomeMunicipio) {
		this.nomeMunicipio = nomeMunicipio;
	}

	public List<Municipio> getMunicipios() {
		this.municipios = municipioRN.listarTodos(getNomeMunicipio());
		return municipios;
	}

	public List<Uf> listarUfs(String query) {
		return municipioRN.listarUfs(query);
	}

}
