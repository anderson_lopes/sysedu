package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.UnidadeProduto;
import com.lmsystem.services.cadastros.UnidadeProdutoRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class UnidadeProdutoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private UnidadeProduto unidadeProduto;

	private String nomeunidade;

	private List<UnidadeProduto> unidadeProdutos;

	@Inject
	protected UnidadeProdutoRN unidadeProdutoRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeunidade("");
		pesquisar();
	}

	public void novo() {
		this.unidadeProduto = new UnidadeProduto();
	}

	public void salvar() {
		try {
			this.unidadeProdutoRN.salvar(this.unidadeProduto);
			this.unidadeProduto = new UnidadeProduto();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.unidadeProduto = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			unidadeProdutoRN.remover(this.unidadeProduto);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.unidadeProduto = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void limpar() {
		this.unidadeProduto = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/estoque/unidade", opcoes, null);
	}

	public void pesquisar() {
		this.getUnidadeProdutos();
	}

	public UnidadeProduto getUnidadeProduto() {
		return unidadeProduto;
	}

	public void setUnidadeProduto(UnidadeProduto unidadeProduto) {
		this.unidadeProduto = unidadeProduto;
	}

	public List<UnidadeProduto> getUnidadeProdutos() {
		this.unidadeProdutos = unidadeProdutoRN.listarTodos(getNomeunidade());
		return unidadeProdutos;
	}

	public String getNomeunidade() {
		return nomeunidade;
	}

	public void setNomeunidade(String nomeunidade) {
		this.nomeunidade = nomeunidade;
	}

}
