package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Deposito;
import com.lmsystem.services.cadastros.DepositoRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class DepositoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Deposito deposito;

	private List<Deposito> depositos;

	private String nomedeposito;

	@Inject
	protected DepositoRN depositoRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomedeposito("");
		pesquisar();
	}

	public void novo() {
		this.setDeposito(new Deposito());
	}

	public void salvar() {
		try {
			depositoRN.salvar(this.deposito);
			this.deposito = new Deposito();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.deposito = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			depositoRN.remover(this.deposito);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.deposito = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void limpar() {
		this.deposito = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/estoque/deposito", opcoes, null);
	}

	public void pesquisar() {
		this.getDepositos();
	}

	public List<Deposito> listarDepositos(String query) {
		return depositoRN.listarTodos(query);
	}

	public Deposito getDeposito() {
		return deposito;
	}

	public void setDeposito(Deposito deposito) {
		this.deposito = deposito;
	}

	public List<Deposito> getDepositos() {
		this.depositos = depositoRN.listarTodos(getNomedeposito());
		return depositos;
	}

	public String getNomedeposito() {
		return nomedeposito;
	}

	public void setNomedeposito(String nomedeposito) {
		this.nomedeposito = nomedeposito;
	}

}
