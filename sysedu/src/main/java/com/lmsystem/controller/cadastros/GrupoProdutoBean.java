package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.GrupoProduto;
import com.lmsystem.services.cadastros.GrupoProdutoRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class GrupoProdutoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private GrupoProduto grupoProduto;

	private String nomegrupo;

	private List<GrupoProduto> grupoProdutos;

	@Inject
	protected GrupoProdutoRN grupoProdutoRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomegrupo("");
		pesquisar();
	}

	public void novo() {
		this.grupoProduto = new GrupoProduto();
	}

	public void salvar() {
		try {
			grupoProdutoRN.salvar(this.grupoProduto);
			this.grupoProduto = new GrupoProduto();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.grupoProduto = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			grupoProdutoRN.remover(this.grupoProduto);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.grupoProduto = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void limpar() {
		this.grupoProduto = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/estoque/grupo-produto", opcoes, null);
	}

	public void pesquisar() {
		this.getGrupoProdutos();
	}

	public GrupoProduto getGrupoProduto() {
		return grupoProduto;
	}

	public void setGrupoProduto(GrupoProduto grupoProduto) {
		this.grupoProduto = grupoProduto;
	}

	public String getNomegrupo() {
		return nomegrupo;
	}

	public void setNomegrupo(String nomegrupo) {
		this.nomegrupo = nomegrupo;
	}

	public List<GrupoProduto> getGrupoProdutos() {
		this.grupoProdutos = grupoProdutoRN.listarTodos(getNomegrupo());
		return grupoProdutos;
	}

	public void setGrupoProdutos(List<GrupoProduto> grupoProdutos) {
		this.grupoProdutos = grupoProdutos;
	}

}
