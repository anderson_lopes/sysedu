package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.TipoLogradouro;
import com.lmsystem.services.cadastros.TipoLogradouroRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class TipoLogradouroBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private TipoLogradouro tipoLogradouro;

	private String nomeTipoLogradouro;

	private List<TipoLogradouro> tipoLogradouros;

	@Inject
	protected TipoLogradouroRN tipoLogradouroRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeTipoLogradouro("");
		pesquisar();
	}

	public void novo() {
		this.tipoLogradouro = new TipoLogradouro();
	}

	public void salvar() {
		try {
			tipoLogradouroRN.salvar(this.tipoLogradouro);
			this.tipoLogradouro = new TipoLogradouro();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.tipoLogradouro = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
		}
	}

	public void remover() {
		try {
			tipoLogradouroRN.remover(this.tipoLogradouro);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.tipoLogradouro = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
		}
	}

	public void limpar() {
		this.tipoLogradouro = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/tipo-logradouro", opcoes, null);
	}

	public void pesquisar() {
		this.getTipoLogradouros();
	}

	public TipoLogradouro getTipoLogradouro() {
		return tipoLogradouro;
	}

	public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}

	public String getNomeTipoLogradouro() {
		return nomeTipoLogradouro;
	}

	public void setNomeTipoLogradouro(String nomeTipoLogradouro) {
		this.nomeTipoLogradouro = nomeTipoLogradouro;
	}

	public List<TipoLogradouro> getTipoLogradouros() {
		this.tipoLogradouros = tipoLogradouroRN.listarTodos(getNomeTipoLogradouro());
		return tipoLogradouros;
	}

	public List<TipoLogradouro> listarTipoLogradouros(String query) {
		return tipoLogradouroRN.listarTipoLogradouros(query);
	}

}
