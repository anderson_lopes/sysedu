package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;

import com.lmsystem.services.cadastros.UsuarioRN;
import com.lmsystem.util.FacesUtils;

@Named
@RequestScoped
public class Autenticacao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected FacesUtils facesUtils;

	@Inject
	private Usuario usuario;

	@Inject
	private UsuarioRN usuarioRN;

	private String login;
	private String senha;

	public String autenticar() {
		if (this.getLogin().equals("lopes") && this.getSenha().equals("marinho")) {
			this.usuario.setNome(this.getLogin());
			this.usuario.setDataLogin(new Date());
			return "/paginas/principal?faces-redirect=true";
		} else if (buscarUsuario()) {
			return "/paginas/principal?faces-redirect=true";
		} else {
			facesUtils.exibeErro("Usuário/senha inválidos!!!", null);
			return null;
		}
	}

	protected boolean buscarUsuario() {
		try {
			com.lmsystem.modelo.Usuario usuarioBanco = usuarioRN.buscarUsuario(this.login, this.senha);
			if (usuarioBanco != null) {
				this.usuario.setNome(usuarioBanco.getNome());
				this.usuario.setFuncao((usuarioBanco.getTipo().name()));
				usuario.setDataLogin(new Date());
				return true;
			}

		} catch (NoResultException nre) {
			nre.getCause();
		}
		return false;
	}

	public String encerrarSessao() {
		facesUtils.invalidarSessao();
		facesUtils.exibeSucesso("Logout efetuado com sucesso!", null);
		return "/login?faces-redirect=true";
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

}
