package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

import com.lmsystem.modelo.Operadora;
import com.lmsystem.services.cadastros.OperadoraRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class OperadoraBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Operadora operadora;

	private String nomeOperadora;

	private List<Operadora> operadoras;

	@Inject
	protected OperadoraRN operadoraRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeOperadora("");
		pesquisar();
	}

	public void novo() {
		this.operadora = new Operadora();
	}

	public void salvar() {
		try {
			operadoraRN.salvar(this.operadora);
			this.operadora = new Operadora();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.operadora = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			operadoraRN.remover(this.operadora);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.operadora = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void limpar() {
		this.operadora = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/operadora", opcoes, null);
	}

	public void pesquisar() {
		this.getOperadoras();
	}

	public void salvarImagem(FileUploadEvent fileUploadEvent) {
		// TODO Implementar salvar logomarca da Operadora de telefonia
	}

	public Operadora getOperadora() {
		return operadora;
	}

	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}

	public String getNomeOperadora() {
		return nomeOperadora;
	}

	public void setNomeOperadora(String nomeOperadora) {
		this.nomeOperadora = nomeOperadora;
	}

	public List<Operadora> getOperadoras() {
		this.operadoras = operadoraRN.listarTodos(getNomeOperadora());
		return operadoras;
	}

}
