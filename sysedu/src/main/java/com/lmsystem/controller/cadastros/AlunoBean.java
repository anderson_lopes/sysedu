package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Bairro;
import com.lmsystem.modelo.Contato;
import com.lmsystem.modelo.Endereco;
import com.lmsystem.modelo.Etnia;
import com.lmsystem.modelo.Municipio;
import com.lmsystem.modelo.Nacionalidade;
import com.lmsystem.modelo.PessoaFisica;
import com.lmsystem.modelo.RacaCor;
import com.lmsystem.modelo.RelPessoaTipo;
import com.lmsystem.modelo.Sexo;
import com.lmsystem.modelo.TipoLogradouro;
import com.lmsystem.services.cadastros.AlunoRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class AlunoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private RelPessoaTipo relPessoaTipo;

	private String nomePessoa;

	private List<RelPessoaTipo> alunos;

	@Inject
	protected AlunoRN alunoRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomePessoa("");
		pesquisar();
	}

	public void novo() {
		this.relPessoaTipo = new RelPessoaTipo();
		this.relPessoaTipo.setPessoaFisica(new PessoaFisica());
		this.getRelPessoaTipo().getPessoaFisica().setContato(new Contato());
		this.getRelPessoaTipo().getPessoaFisica().setEndereco(new Endereco());
	}

	public void salvar() {
		try {
			alunoRN.salvar(getRelPessoaTipo());
			pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.relPessoaTipo = null;
			iniciar();
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", e.getMessage());
		}
	}

	public void remover() {
		try {
			alunoRN.remover(this.getRelPessoaTipo());
			this.pesquisar();
			facesUtils.exibeSucesso("registro removido com sucesso!", null);
			this.relPessoaTipo = null;
			iniciar();
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
		}
	}

	public void limpar() {
		this.relPessoaTipo = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		opcoes.put("resizable", false);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/academico/aluno", opcoes, null);
	}

	public void pesquisar() {
		this.getAlunos();
	}

	public void buscarCEP() {
		this.relPessoaTipo = alunoRN.buscarCEP(getRelPessoaTipo());
	}

	public RelPessoaTipo getRelPessoaTipo() {
		return relPessoaTipo;
	}

	public void setRelPessoaTipo(RelPessoaTipo relPessoaTipo) {
		this.relPessoaTipo = relPessoaTipo;
	}

	public String getNomePessoa() {
		return nomePessoa;
	}

	public void setNomePessoa(String nomePessoa) {
		this.nomePessoa = nomePessoa;
	}

	public List<Sexo> listarSexos(String query) {
		return alunoRN.listarSexos(query);
	}

	public List<RacaCor> listarRacasCor(String query) {
		return alunoRN.listarRacasCor(query);
	}

	public List<Etnia> listarEtnias(String query) {
		return alunoRN.listarEtnias(query);
	}

	public List<Municipio> listarMunicipios(String query) {
		return alunoRN.listarMunicipios(query);
	}

	public List<Nacionalidade> listarNacionalidades(String query) {
		return alunoRN.ListarNacionalidades(query);
	}

	public List<TipoLogradouro> listarTiposLogradouro(String query) {
		return alunoRN.listarTipoLogradouros(query);
	}

	public List<Bairro> listarBairros(String query) {
		return alunoRN.listarBairros(query);
	}

	public List<RelPessoaTipo> getAlunos() {
		this.alunos = alunoRN.listarTodos(getNomePessoa());
		return alunos;
	}
}
