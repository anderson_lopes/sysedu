package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.GrupoProduto;
import com.lmsystem.modelo.SubGrupoProduto;
import com.lmsystem.services.cadastros.GrupoProdutoRN;
import com.lmsystem.services.cadastros.SubGrupoProdutoRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class SubGrupoProdutoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private SubGrupoProduto subgrupoProduto;

	private List<SubGrupoProduto> subgrupoProdutos;

	private String nomesubgrupo;

	@Inject
	protected SubGrupoProdutoRN subGrupoProdutoRN;

	@Inject
	protected GrupoProdutoRN grupoProdutoRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomesubgrupo("");
		pesquisar();
	}

	public void novo() {
		this.subgrupoProduto = new SubGrupoProduto();
	}

	public void salvar() {
		try {
			subGrupoProdutoRN.salvar(this.subgrupoProduto);
			this.subgrupoProduto = new SubGrupoProduto();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.subgrupoProduto = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			subGrupoProdutoRN.remover(this.subgrupoProduto);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.subgrupoProduto = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/estoque/subgrupo-produto", opcoes, null);
	}

	public void limpar() {
		this.subgrupoProduto = null;
	}

	public void pesquisar() {
		this.getSubGrupoProdutos();
	}

	public SubGrupoProduto getSubgrupoProduto() {
		return subgrupoProduto;
	}

	public void setSubgrupoProduto(SubGrupoProduto subgrupoProduto) {
		this.subgrupoProduto = subgrupoProduto;
	}

	public List<SubGrupoProduto> getSubGrupoProdutos() {
		this.subgrupoProdutos = subGrupoProdutoRN.listarTodos(getNomesubgrupo());
		return subgrupoProdutos;
	}

	public List<SubGrupoProduto> listarSubGrupoProdutos() {
		return subGrupoProdutoRN.listarTodos(getNomesubgrupo());
	}

	public List<GrupoProduto> listarGrupoProdutos(String query) {
		return grupoProdutoRN.listarTodos(query);
	}

	public String getNomesubgrupo() {
		return nomesubgrupo;
	}

	public void setNomesubgrupo(String nomesubgrupo) {
		this.nomesubgrupo = nomesubgrupo;
	}

}
