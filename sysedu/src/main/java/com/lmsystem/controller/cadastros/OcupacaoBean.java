package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Ocupacao;
import com.lmsystem.services.cadastros.OcupacaoRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class OcupacaoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Ocupacao ocupacao;

	private String nomeOcupacao;

	private List<Ocupacao> ocupacoes;

	@Inject
	protected OcupacaoRN ocupacaoRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeOcupacao("");
		pesquisar();
	}

	public void novo() {
		this.ocupacao = new Ocupacao();
	}

	public void salvar() {
		try {
			ocupacaoRN.salvar(this.ocupacao);
			this.ocupacao = new Ocupacao();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.ocupacao = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			ocupacaoRN.remover(this.ocupacao);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.ocupacao = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void limpar() {
		this.ocupacao = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/ocupacao", opcoes, null);
	}

	public void pesquisar() {
		this.getOcupacoes();
	}

	public Ocupacao getOcupacao() {
		return ocupacao;
	}

	public void setOcupacao(Ocupacao ocupacao) {
		this.ocupacao = ocupacao;
	}

	public String getNomeOcupacao() {
		return nomeOcupacao;
	}

	public void setNomeOcupacao(String nomeOcupacao) {
		this.nomeOcupacao = nomeOcupacao;
	}

	public List<Ocupacao> getOcupacoes() {
		this.ocupacoes = ocupacaoRN.listarTodos(getNomeOcupacao());
		return ocupacoes;
	}

}
