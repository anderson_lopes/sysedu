package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Setor;
import com.lmsystem.services.cadastros.SetorRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class SetorBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Setor setor;

	private List<Setor> setores;

	private String nomesetor;

	@Inject
	protected SetorRN setorRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomesetor("");
		pesquisar();
	}

	public void novo() {
		this.setor = new Setor();
	}

	public void salvar() {
		try {
			setorRN.salvar(this.setor);
			this.setor = new Setor();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.setor = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
		}
	}

	public void remover() {
		try {
			setorRN.remover(this.setor);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.setor = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
		}
	}

	public void limpar() {
		this.setor = null;
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/setor", opcoes, null);
	}

	public void pesquisar() {
		this.getSetores();
	}

	public List<Setor> listarSetores(String query) {
		return setorRN.listarTodos(query);
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public List<Setor> getSetores() {
		this.setores = setorRN.listarTodos(getNomesetor());
		return setores;
	}

	public String getNomesetor() {
		return nomesetor;
	}

	public void setNomesetor(String nomesetor) {
		this.nomesetor = nomesetor;
	}

}
