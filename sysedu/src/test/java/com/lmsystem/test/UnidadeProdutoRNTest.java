package com.lmsystem.test;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lmsystem.modelo.UnidadeProduto;
import com.lmsystem.util.EntityManagerProducer;
import com.lmsystem.util.Transacional;

public class UnidadeProdutoRNTest {

	private EntityManagerProducer entityManagerProducer;
	private EntityManager em;
	private UnidadeProduto unidadeProduto;

	@Before
	public void inicia() {
		entityManagerProducer = new EntityManagerProducer();
		em = entityManagerProducer.createEntityManager();
		unidadeProduto = new UnidadeProduto();
	}

	@Test
	@Transacional
	public void deveGuardarUnidadeProduto() {

		this.unidadeProduto.setFator(100);
		this.unidadeProduto.setSigla("TEST");
		this.unidadeProduto.setUnidade("TESTE");

		em.persist(this.unidadeProduto);
	}

	@Test
	@Transacional
	public void deveremoverUnidadeProduto() {
		em.remove(this.unidadeProduto);
	}

	@After
	public void finaliza() {
		entityManagerProducer.closeEntityManager(em);
	}

}
