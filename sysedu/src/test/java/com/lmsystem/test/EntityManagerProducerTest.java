package com.lmsystem.test;

import javax.persistence.EntityManager;

import org.junit.Test;

import com.lmsystem.util.EntityManagerProducer;

public class EntityManagerProducerTest {

	@Test
	public void gerEntityManager() {
		EntityManagerProducer entityManagerProducer = new EntityManagerProducer();
		EntityManager entityManager = entityManagerProducer.createEntityManager();
		entityManagerProducer.closeEntityManager(entityManager);
	}

}
