package com.lmsystem.test;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.lmsystem.modelo.AnoLetivo;
import com.lmsystem.util.EntityManagerProducer;
import com.lmsystem.util.Transacional;

public class AnoLetivoBeanTest {

	private EntityManagerProducer entityManagerProducer;
	private EntityManager em;
	private AnoLetivo anoLetivo;

	@Before
	public void inicia() {
		entityManagerProducer = new EntityManagerProducer();
		em = entityManagerProducer.createEntityManager();
		anoLetivo = new AnoLetivo();
	}

	@Test
	@Transacional
	public void deveGuardarAnoLetivo() {

		this.anoLetivo.setAnoletivo("2017");
		this.anoLetivo.setDias(200);
		this.anoLetivo.setDatainicio(Calendar.getInstance().getTime());
		this.anoLetivo.setDatafim(Calendar.getInstance().getTime());

		em.merge(this.anoLetivo);
	}

	@Test
	public void deveRetornarTresAnosLetivos() {
		assertEquals(3, em.createNamedQuery("AnoLetivo.findAll").getResultList().size());
	}

	@Ignore
	@Test
	public void deveremoverAnoLetivo() {
		em.remove(this.anoLetivo);
	}

	@After
	public void finaliza() {
		entityManagerProducer.closeEntityManager(em);
	}

}
